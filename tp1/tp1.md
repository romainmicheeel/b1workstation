# TP n°1

# I. Self-footprinting

## Host OS

pour déterminer les infos de la machine telles que :

- le nom de la machine

```
$env:computername
LAPTOP-0PRP7B52
```

- OS et version
  on utilise pour connaitre l'os

```
$(Get-WmiObject Win32_OperatingSystem).Caption
"Microsoft Windows 10 Famille"
```

et on utilise pour connaitre la version

```
$(Get-WmiObject Win32_OperatingSystem).Version
10.0.19041
```

- architecture processeur (32-bit, 64-bit, ARM, etc)

```
$(wmic os get osarchitecture)[2]
64-bit
```

- quantité RAM et modèle de la RAM

```
Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize

Manufacturer Banklabel Configuredclockspeed Devicelocator    Capacity Serialnumber
------------ --------- -------------------- -------------    -------- ------------
SK Hynix     BANK 0                    2933 ChannelA-DIMM0 8589934592 43D2634B
SK Hynix     BANK 2                    2933 ChannelB-DIMM0 8589934592 43D263AD
```

## Devices

- la marque et le modèle de votre processeur

```
$systeminfo
[...]
Processeur(s):   1 processeur(s) installé(s).
         [01] : Intel64 Family 6 Model 165 Stepping 2 GenuineIntel ~2592 MHz
[...]
```

-le nombre de processeurs

```
$wmic path win32_VideoController get name
```

- la marque et le modèle :

-de la carte graphique

```
$wmic path win32_VideoController get name
Name
NVIDIA GeForce RTX 2060
Intel(R) UHD Graphics
```

- du disque dur

-la marque et le modèle de(s) disque(s) dur(s)

```
$wmic diskdrive get Model,Size
Model                           Size
WDC PC SN530 SDBPNPZ-1T00-1014  1024203640320


```

-les différentes partitions de(s) disque(s) dur(s) et leur fonction (type):  
System est là ou est l'os du pc  
Reserved est réservé à la mémoire ram  
Basic est le lieu de stockage des données utilisateur  
Recover est une partition de secours au cas où le fonctionnement du pc est corrompu

```
$Get-Partition

   DiskPath :
\\?\scsi#disk&ven_nvme&prod_wdc_pc_sn530_sdb#4&26a8527d&0&020000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                       Size Type
---------------  ----------- ------                                       ---- ----
1                           1048576                                    100 MB System
2                           105906176                                   16 MB Reserved
3                C           122683392                               952.75 GB Basic
4                           1023135449088                                1 GB Recovery
```

-le système de fichier du disque (ici a la fin de la ligne "GPT")

```
$Get-Disk
Number Friendly   Serial Number                    HealthStatus         OperationalStatus      Total Size Partition
       Name                                                                                               Style
------ ---------- -------------                    ------------         -----------------      ---------- ----------
0      WDC PC ... E823_8FA6_BF53_0001_001B_444A... Healthy              Online                  953.87 GB GPT
```

et ici le systeme de ficher des partitions

```
PS C:\WINDOWS\system32> diskpart

Microsoft DiskPart version 10.0.19041.1

Copyright (C) Microsoft Corporation.
Sur l’ordinateur : LAPTOP-0PRP7B52

DISKPART> select disk 0

Le disque 0 est maintenant le disque sélectionné.

DISKPART> select partition 1

La partition 1 est maintenant la partition sélectionnée.

DISKPART> detail partition
[...]
DISKPART> detail partition

Partition 1
Type    : c12a7328-f81f-11d2-ba4b-00a0c93ec93b
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 1048576

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 2         ESP          FAT32  Partition    100 M   Sain       Système
  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 2         ESP          FAT32  Partition    100 M   Sain       Système

DISKPART> select part 2

La partition 2 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 2
Type    : e3c9e316-0b5c-4db8-817d-f92df00215ae
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 105906176

Il n’y a pas de volume associé avec cette partition.

Partition 3
Type    : ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
Masqué  : Non
Requis  : Non
Attrib  : 0000000000000000
Décalage en octets : 122683392

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 0     C   Acer         NTFS   Partition    952 G   Sain       Démarrag

DISKPART> detail partition

Partition 4
Type    : de94bba4-06d1-4d40-a16a-bfd50179d6ac
Masqué  : Non
Requis  : Oui
Attrib  : 0X8000000000000001
Décalage en octets : 1023135449088

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 3                      NTFS   Partition   1024 M   Sain       Masqué
```

## Users

-la liste complète des utilisateurs de la machine

```
$Get-LocalUser

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
DefaultAccount     False   Compte utilisateur géré par le système.
Invité             False   Compte d’utilisateur invité
romai              True
Romaing            True
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defende...

```

-le nom de l'utilisateur qui est full admin sur la machine

```
 $objSID = New-Object System.Security.Principal.SecurityIdentifier("S-1-5-20")
>> $objAccount = $objSID.Translate([System.Security.Principal.NTAccount])
>> $objAccount.Value
AUTORITE NT\SERVICE RÉSEAU
```

## Processus

- 5 process expliqués

```
$get-process

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
   [...]
    266      13     2496      10684             22700   0 svchost
   6259       0      252      27700                 4   0 System
    763      36    29844       2420       0,77  11096  11 SystemSettings
    313      43    10084      20544       2,30    708  11 taskhostw
    555      23    14672      46664       5,31  16120  11 TextInputHost
    121       8     1620       7892             15972  11 unsecapp
    119       8     1440       7772             21396  11 unsecapp
    185      11     1800       3236               888   0 wininit
    276      12     2704      10940             10884  11 winlogon
   1124      74    58740       2640       1,80  21184  11 WinStore.App
    102       7     1388       1964             18520   0 wlanext
     24       4      420       2528              9136   0 WMIC
    200      14     4612       8204              5820   0 WmiPrvSE
    384      18    19412      21196              6028   0 WmiPrvSE
    139       9     2968       8048             14880   0 WmiPrvSE
    208       7     1576       1928              1488   0 WUDFHost
```

**-svchost.exe** un processus qui consiste à charger des bibliothèques de liens dynamiques (DLL)  
**-wininit.exe** est responsable de l'exécution du processus d'initialisation Windows  
**-winlogon.exe** est est un processus générique de Windows NT/2000/XP servant à gérer l'ouverture et la fermeture des sessions.  
**-SystemSettings.exe** est un processus de confiance de Microsoft. Le fichier a une signature numérique.  
**-smss.exe** est le gestionnaire de session de Windows.
Lors du démarrage de Windows, le noyau de Windows (ntoskrnl.exe) charge ce dernier.

```
A partir de là, smss.exe effectue les opérations suivantes :
    Crée des variables d'environnement
    Charge la clé BootExecute ce qui permet de lancer un chkdsk au démarrage de Windows
    Démarre les modes noyau et utilisateur du sous-système Win32. Ce sous-système comprend
        win32k.sys (mode noyau),
        winsrv.dll (mode utilisateur)
        csrss.exe (mode utilisateur).
    Crée des mappages de périphériques DOS(COMX, LPT, CON, etc
    Crée des fichiers de pagination de mémoire virtuelle
    Lance winlogon.exe, le gestionnaire de connexion Windows

```

**-Conhost** se charge de faire le tampon entre l'application l'affichage et les parties plus basses de Windows (win2k.sys) pour qu'en cas de crash ou problème sur l'application utilisant Conhost les deux processus peuvent se fermer, sans créer d'incident sur Windows mais simplement sur l'application qui l'utilise.

- les process lancés en full admin  
  les process lancés en full admin sont ceux avec l'username "AUTORITE NT\Système"

```
Get-Process -IncludeUserName

Handles      WS(K)   CPU(s)     Id UserName               ProcessName
-------      -----   ------     -- --------               -----------
    909      10908     2,89   9504 LAPTOP-0PRP7B52\romai  ACCStd
    131       2292     0,33   3948 AUTORITE NT\Système    ACCSvc
    549      72424    19,69  18072 LAPTOP-0PRP7B52\romai  ACEStd
    525      38808     1,20  22064 LAPTOP-0PRP7B52\romai  ApplicationFrameHost
    159       1240     0,03  10892 AUTORITE NT\Système    AppVShNotify
    187      19000     0,59  21832 AUTORITE NT\SERVICE... audiodg
    325       2560     1,20   3996 AUTORITE NT\Système    Bridge_Service
    846       2428     1,14  18256 LAPTOP-0PRP7B52\romai  commsapps
     99       1088     0,08   1756 LAPTOP-0PRP7B52\romai  conhost
    153       1844     0,27   6856 AUTORITE NT\Système    conhost
    153       2092     5,02   7080 AUTORITE NT\Système    conhost
    261      16924     0,17  11816 LAPTOP-0PRP7B52\romai  conhost
     96       1140     0,00  19200 AUTORITE NT\Système    conhost
    310      18568    17,14  20300 LAPTOP-0PRP7B52\romai  conhost
    719      66628     1,45   8292 LAPTOP-0PRP7B52\romai  Cortana
[...]
```

## NetWork

- cartes réseau de votre machine  
  la carte 01 est la carte qui permet au pc de se connecter en wifi  
  la carte 02 est la carte qui recoit les infos d'un cable ethernet
  la carte 03 est l'adaptateur ethernet pour que la machine virtuelle soit en lien avec la carte physique 02

```
$systeminfo
[...]Carte(s) réseau:                            3 carte(s) réseau installée(s).
                                            [01]: Intel(R) Wi-Fi 6 AX201 160MHz
                                                  Nom de la connexion : Wi-Fi
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        192.168.0.254
                                                  Adresse(s) IP
                                                  [01]: 192.168.0.46
                                                  [02]: fe80::d9dc:8dd5:a8c2:e7e0
                                                  [03]: 2a01:e0a:12c:2ee0:f129:64ff:4fbd:e1b0
                                                  [04]: 2a01:e0a:12c:2ee0:d9dc:8dd5:a8c2:e7e0
                                            [02]: Killer E2600 Gigabit Ethernet Controller
                                                  Nom de la connexion : Ethernet
                                                  État :                Support déconnecté
                                            [03]: VirtualBox Host-Only Ethernet Adapter
                                                  Nom de la connexion : VirtualBox Host-Only Network
                                                  DHCP activé :         Non
                                                  Adresse(s) IP
                                                  [01]: 192.168.56.1
                                                  [02]: fe80::1c3f:902b:7f7e:6c79
[...]
```

- ports TCP et UDP en utilisation

```
PS C:\WINDOWS\system32> netstat -ab

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            LAPTOP-0PRP7B52:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:808            LAPTOP-0PRP7B52:0      LISTENING
 [OneApp.IGCC.WinService.exe]
  TCP    0.0.0.0:5040           LAPTOP-0PRP7B52:0      LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:7680           LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          LAPTOP-0PRP7B52:0      LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          LAPTOP-0PRP7B52:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          LAPTOP-0PRP7B52:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          LAPTOP-0PRP7B52:0      LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49670          LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:50009          LAPTOP-0PRP7B52:0      LISTENING
 [NortonSecurity.exe]
  TCP    0.0.0.0:50010          LAPTOP-0PRP7B52:0      LISTENING
 [NortonSecurity.exe]
  TCP    127.0.0.1:3213         LAPTOP-0PRP7B52:0      LISTENING
 [OriginWebHelperService.exe]
  TCP    127.0.0.1:55989        LAPTOP-0PRP7B52:0      LISTENING
 [Bridge_Service.exe]
  TCP    127.0.0.1:56989        LAPTOP-0PRP7B52:0      LISTENING
 [Bridge_Service.exe]
  TCP    127.0.0.1:58537        LAPTOP-0PRP7B52:0      LISTENING
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:58537        LAPTOP-0PRP7B52:62326  ESTABLISHED
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:62326        LAPTOP-0PRP7B52:58537  ESTABLISHED
 [NVIDIA Share.exe]
  TCP    127.0.0.1:65001        LAPTOP-0PRP7B52:0      LISTENING
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        LAPTOP-0PRP7B52:65426  ESTABLISHED
 [nvcontainer.exe]
  TCP    127.0.0.1:65426        LAPTOP-0PRP7B52:65001  ESTABLISHED
 [nvcontainer.exe]
  TCP    192.168.0.46:139       LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.0.46:62407     40.67.251.132:https    ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.0.46:62492     40.67.254.36:https     ESTABLISHED
 [OneDrive.exe]
  TCP    192.168.0.46:63194     lhr25s01-in-f68:https  TIME_WAIT
  TCP    192.168.0.46:63197     ec2-18-179-225-220:https  TIME_WAIT
  TCP    192.168.0.46:63205     ec2-52-35-6-89:https   TIME_WAIT
  TCP    192.168.0.46:63206     ec2-52-28-48-75:https  TIME_WAIT
  TCP    192.168.0.46:63207     ec2-35-157-96-243:https  TIME_WAIT
  TCP    192.168.0.46:63217     server-99-84-10-95:https  TIME_WAIT
  TCP    192.168.0.46:63218     server-13-249-11-89:https  TIME_WAIT
  TCP    192.168.0.46:63219     ec2-3-121-33-233:https  TIME_WAIT
  TCP    192.168.0.46:63220     ec2-3-121-33-233:https  TIME_WAIT
  TCP    192.168.0.46:63229     192.0.73.2:https       TIME_WAIT
  TCP    192.168.0.46:63234     ec2-44-241-216-61:https  TIME_WAIT
  TCP    192.168.0.46:63237     server-13-224-193-10:https  TIME_WAIT
  TCP    192.168.0.46:63239     162.125.67.1:https     TIME_WAIT
  TCP    192.168.0.46:63240     par21s17-in-f10:https  TIME_WAIT
  TCP    192.168.0.46:63245     a88-221-113-56:http    ESTABLISHED
 [NortonSecurity.exe]
  TCP    192.168.0.46:63249     a104-69-88-50:http     ESTABLISHED
 [NortonSecurity.exe]
  TCP    192.168.0.46:63259     93.184.220.29:http     TIME_WAIT
  TCP    192.168.0.46:63260     93.184.220.29:http     TIME_WAIT
  TCP    192.168.0.46:63261     ec2-18-179-225-220:https  TIME_WAIT
  TCP    192.168.0.46:63264     13.107.5.93:https      TIME_WAIT
  TCP    192.168.0.46:63265     13.69.68.34:https      TIME_WAIT
  TCP    192.168.0.46:63268     51.144.164.215:https   TIME_WAIT
  TCP    192.168.0.46:63274     13.69.68.34:https      TIME_WAIT
  TCP    192.168.0.46:63275     13.69.68.34:https      TIME_WAIT
  TCP    192.168.0.46:63276     13.69.68.34:https      TIME_WAIT
  TCP    192.168.0.46:63277     13.69.68.34:https      TIME_WAIT
  TCP    192.168.0.46:63278     13.69.68.34:https      TIME_WAIT
  TCP    192.168.0.46:63279     13.69.68.34:https      TIME_WAIT
  TCP    192.168.0.46:63282     152.199.19.160:https   TIME_WAIT
  TCP    192.168.0.46:63285     13.107.6.175:https     TIME_WAIT
  TCP    192.168.0.46:63287     51.138.106.75:https    ESTABLISHED
  CDPUserSvc_1ec40534
 [svchost.exe]
  TCP    192.168.56.1:139       LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:135               LAPTOP-0PRP7B52:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    [::]:445               LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:808               LAPTOP-0PRP7B52:0      LISTENING
 [OneApp.IGCC.WinService.exe]
  TCP    [::]:7680              LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49664             LAPTOP-0PRP7B52:0      LISTENING
 [lsass.exe]
  TCP    [::]:49665             LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             LAPTOP-0PRP7B52:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    [::]:49667             LAPTOP-0PRP7B52:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    [::]:49668             LAPTOP-0PRP7B52:0      LISTENING
 [spoolsv.exe]
  TCP    [::]:49670             LAPTOP-0PRP7B52:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:50010             LAPTOP-0PRP7B52:0      LISTENING
 [NortonSecurity.exe]
  TCP    [::1]:60224            LAPTOP-0PRP7B52:0      LISTENING
 [jhi_service.exe]
  TCP    [2a01:e0a:12c:2ee0:f129:64ff:4fbd:e1b0]:54063  [2a01:111:f400:31ab::2]:https  ESTABLISHED
 [SearchApp.exe]
  TCP    [2a01:e0a:12c:2ee0:f129:64ff:4fbd:e1b0]:54070  [2a01:111:f100:2000::a83e:3185]:https  ESTABLISHED
 [NortonSecurity.exe]
  TCP    [2a01:e0a:12c:2ee0:f129:64ff:4fbd:e1b0]:54071  [2a01:111:f100:3000::a83e:191f]:https  ESTABLISHED
 [NortonSecurity.exe]
  UDP    0.0.0.0:123            *:*
  W32Time
 [svchost.exe]
  UDP    0.0.0.0:500            *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:49666          *:*
 [nvcontainer.exe]
  UDP    0.0.0.0:55160          *:*
[svchost.exe]
  UDP    127.0.0.1:10150        *:*
[svchost.exe]
  UDP    127.0.0.1:56003        *:*
  NlaSvc
 [svchost.exe]
  UDP    127.0.0.1:64181        *:*
 [nvcontainer.exe]
  UDP    127.0.0.1:64924        *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.0.46:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.0.46:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.0.46:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.0.46:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.0.46:5353      *:*
 [nvcontainer.exe]
  UDP    192.168.0.46:64923     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.56.1:5353      *:*
 [nvcontainer.exe]
  UDP    192.168.56.1:64922     *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::]:123               *:*
  W32Time
 [svchost.exe]
  UDP    [::]:500               *:*
  IKEEXT
 [svchost.exe]
  UDP    [::]:4500              *:*
  IKEEXT
 [svchost.exe]
  UDP    [::]:5353              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:5355              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:49667             *:*
 [nvcontainer.exe]
  UDP    [::1]:1900             *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:5353             *:*
 [nvcontainer.exe]
  UDP    [::1]:64921            *:*
  SSDPSRV
 [svchost.exe]
  UDP    [2a01:e0a:12c:2ee0:d9dc:8dd5:a8c2:e7e0]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [2a01:e0a:12c:2ee0:f129:64ff:4fbd:e1b0]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [fe80::1c3f:902b:7f7e:6c79%41]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::1c3f:902b:7f7e:6c79%41]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [fe80::1c3f:902b:7f7e:6c79%41]:64919  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::d9dc:8dd5:a8c2:e7e0%18]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::d9dc:8dd5:a8c2:e7e0%18]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [fe80::d9dc:8dd5:a8c2:e7e0%18]:64920  *:*
  SSDPSRV
 [svchost.exe]
```

déterminer quel programme tourne derrière chacun des ports
expliquer la fonction de chacun de ces -**svchost.exe** un processus qui consiste à charger des bibliothèques de liens dynamiques (DLL), elles conservent un grand nombre des renseignements indispenables et des ressources nécessaires pour utiliser les applications installées dans l'ordinateur.  
**-nvcontainer.exe** est un un collecteur d'informations de NVIDIA qui récupère des infos techniques de l'ordinateur pour les utiliser
**-NortonSecurity.exe** sont des processus de mon antivirus, il communique avec internet pour être informé des dernières mises a jour de sécurité
**-jhi_service.exe** est un processus de Intel qui est la marque de mon processeur qui permet la détéction et la protection de failles de sécurité décrites dans les conseils d'intel security, il communique donc avec intel pour être informé des dernières mises a jour
**-spoolsv.exe** est un processus générique de Windows NT/2000/XP servant à mettre en mémoire (file d'attente) les travaux d'impression, il est en lien avec mon imprimante via mon réseau internet
**-lsass.exe** est un processus système natif de Windows 2000/XP gérant les mécanismes de sécurité locale et d'authentification des utilisateurs via le service WinLogon. Il s'agit ainsi d'un serveur local d'authentification servant, en cas d'authentification réussie, à créer un jeton d'accès permettant de lancer la session.
**-OneApp.IGCC.WinService.exe** est un driver de la carte Graphique Intel et communique la carte de loopback
**-OneDrive.exe** est un service de cloud qui stoque certaines de mes données sur leurs serveurs
**-NVIDIA Share.exe et NVIDIA Web Helper.exe** sont des processus qui permettent de partager des sessions de jeu a tout moment et a avoir des informations sur le logiciel NVIDIA, ils communiquent avec la carte de loopback
**-Bridge_Service.exe** est un processus de goTrustID qui est un logiciel de base sur mon pc qui permet de dévérouiller le pc avec un téléphone, il communique avec la carte de loopback

# II. Scripting

### script n°1

```
#Romain Michel
#19/12/2020
#ce script affiche un résumé de l'os, liste les utilisateurs de la machine, affiche le ping moyen vers 8.8.8.8 et montre la bande passante du pc sur le réseau
$rep_time = (Test-Connection -ComputerName "8.8.8.8" -Count 4  | measure-Object -Property ResponseTime -Average).average
$criteria = "Type='software' and IsAssigned=1 and IsHidden=0 and IsInstalled=0"#checkupdate
$searcher = (New-Object -COM Microsoft.Update.Session).CreateUpdateSearcher()#
$updates = $searcher.Search($criteria).Updates#
Write-Output "-----------------------------"
Write-Output "FootPrint info"
Write-Output "-----------------------------"
$pcname = $env:computername
Write-Output "Le nom du PC est : $pcname"
$env:HostIP = ( ` Get-NetIPConfiguration | ` #on choppe l'ip
    Where-Object { `
            $_.IPv4DefaultGateway -ne $null `
            -and `
            $_.NetAdapter.Status -ne "Disconnected" `
    } `
).IPv4Address.IPAddress
$ip = $env:HostIP #on l'affiche
Write-Output "ip principale : $ip"
$osName = $(Get-WmiObject Win32_OperatingSystem).Caption
$osVer = $(Get-WmiObject Win32_OperatingSystem).Version
Write-Output "OS et sa version : $osName $osVer"
$startTime = $(gcim Win32_OperatingSystem).LastBootUpTime
Write-Output "date et heure de l'allumage : $startTime"

if ($updates.Count -ne 0) {
    $osUpdated = "le systeme n'est pas a jour :/"
}
else {
    $osUpdated = "le systeme est a jour :)"
}
Write-Output "est-ce que l'os est a jour : $osUpdated"

$Taille_RAM_MAX = [STRING]((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory / 1GB)
$Taille_RAM_LIBRE = [String]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory / 1MB)
$Taille_RAM_UTILISE = [STRING]($Taille_RAM_MAX - $Taille_RAM_LIBRE) + " GB"
$ramLibre = systeminfo | select-object -index 27
$ramLibre = $ramLibre.Split(' ').Trim() -join ''
$ramLibre = $ramLibre.Split(':').Trim() -join ' '
Write-Output "Memoire virtuelle utilisee $Taille_RAM_UTILISE"
Write-Output "$ramLibre"
$TotalSpace = [Math]::Round((Get-Volume -DriveLetter 'C').Size / 1GB)
$FreeSpace = [Math]::Round((Get-Volume -DriveLetter 'C').SizeRemaining / 1GB)
$UsedSpace = ($TotalSpace - $FreeSpace)
Write-Output "Espace disque dispo $FreeSpace GB"
Write-Output "Espace disque utilise $usedSpace GB"
Write-Output "-----------------------------"
Write-Output "Net info"
Write-Output "-----------------------------"
Write-Output "le ping vers 8.8.8.8 est de $rep_time ms" #NB : si vous êtes dans les locaux d'YNOV, le ping vers 8.8.8.8 est impossible. Vous pouvez remplacer par 10.33.3.253 (cette IP n'est joignable QUE depuis le réseau d'YNOV)

$startTime = get-date #celui la est bricolé par bibi mais par la suite j'ai trouvé un truc plus simple que j'utilise pour l'upload test
$endTime = $startTime.AddSeconds(10)
$timeSpan = new-timespan $startTime $endTime
$count = 0

while ($timeSpan -gt 0){
   $colInterfaces = Get-WmiObject -class Win32_PerfFormattedData_Tcpip_NetworkInterface |select BytesTotalPersec
   foreach ($interface in $colInterfaces) {
      $bitsPerSec += $interface.BytesTotalPersec /8
      $count++
   }
   Start-Sleep -Seconds 1
   # recalculate the remaining time
   $timeSpan = new-timespan $(Get-Date) $endTime
}
$MoyDownload=($bitsPerSec/$count)/1000
Write-Output "download speed average within 10s : $MoyDownload Mb/s"


$DownloadURL = "https://bintray.com/ookla/download/download_file?file_path=ookla-speedtest-1.0.0-win64.zip"
$DownloadLocation = "$($Env:ProgramData)\SpeedtestCLI"
try {
    $TestDownloadLocation = Test-Path $DownloadLocation
    if (!$TestDownloadLocation) {
        new-item $DownloadLocation -ItemType Directory -force
        Invoke-WebRequest -Uri $DownloadURL -OutFile "$($DownloadLocation)\speedtest.zip"
        Expand-Archive "$($DownloadLocation)\speedtest.zip" -DestinationPath $DownloadLocation -Force
    }
}
catch {
    write-host "The download and extraction of SpeedtestCLI failed. Error: $($_.Exception.Message)"
    exit 1
}
$SpeedtestResults = & "$($DownloadLocation)\speedtest.exe" --format=json --accept-license --accept-gdpr
$SpeedtestResults = $SpeedtestResults | ConvertFrom-Json
$UploadSpeed = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)

Write-Output "upload speed : $UploadSpeed Mb/s"
```

**Output :**

```
PS F:\Ywork> f:\Ywork\b1workstation\tp1\script\script.ps1
-----------------------------
FootPrint info
-----------------------------
Le nom du PC est : PANDAINDUSTRIES
ip principale : 192.168.0.31
OS et sa version : Microsoft Windows 10 Famille 10.0.18363
date et heure de l'allumage : 08/24/2020 00:14:33
est-ce que l'os est a jour : le systeme n'est pas a jour :/
Memoire virtuelle utilisee 6.32318878173824 GB
Mémoire virtuelle disponible 10 828Mo
Espace disque dispo 45 GB
Espace disque utilise 65 GB
-----------------------------
Net info
-----------------------------
le ping vers 8.8.8.8 est de 9.5 ms
download speed average within 10s : 15.7830908186964 Mb/s
upload speed : 93.53 Mb/s
```

### Script n°2

```
#Romain Michel
#22/10/2020
#ce script peut effectuer des actions après un certain temps en fonction des arguments (ici lock et shutdown)

if ($args[0] -match "lock") {
    Start-Sleep -Seconds $args[1] #timer
    rundll32.exe user32.dll, LockWorkStationlok
}
elseif ($args -match "shutdown") {
    shutdown /s /t $args[1] #/t pour le temps
}
else {
    echo "wrong arguments"
}
```

# III. Gestion de softs

un gestionnaire de paquets se connecte à des dépôts en ligne dans lesquels sont stockés les logiciels sous forme de paquets.L'utiliser permet de savoir si le programme est signé et donc si l'origine peut être fiable; les logiciels également sont dépourvus de spywares, malwares car ils se trouvent sur un dépôt/serveur qui va vérifier l'intégrité des logiciels déposés. c'est mieux parce-que certains logiciels d'internet ralentissent le pc en utilisant sa puissance de calcul ou/et peuvent espionner des données confidentielles

**Liste des packages installés :**

```
PS C:\WINDOWS\system32> choco list --local-only
Chocolatey v0.10.15
chocolatey 0.10.15
chocolatey-core.extension 1.3.5.1
chocolateygui 0.17.2
DotNet4.5.2 4.5.2.20140902
lessmsi 1.7.0
webpicmd 7.1.50430.20141001
6 packages installed.
```

**Origine des paquets :**

```
PS C:\WINDOWS\system32> choco list --local-only --verbose
Chocolatey v0.10.15
chocolatey 0.10.15
 Title: Chocolatey | Published: 22/10/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: n/a
 Tags: nuget apt-get machine repository chocolatey
 Software Site: https://github.com/chocolatey/choco
 Software License: https://raw.githubusercontent.com/chocolatey/choco/master/LICENSE
 Summary: Chocolatey is the package manager for Windows (like apt-get but for Windows)
 Description: Chocolatey is a package manager for Windows (like apt-get but for Windows). It was designed to be a decentralized framework for quickly installing applications and tools that you need. It is built on the NuGet infrastructure currently using PowerShell as its focus for delivering packages from the distros to your door, err computer.

  Chocolatey is brought to you by the work and inspiration of the community, the work and thankless nights of the [Chocolatey Team](https://github.com/orgs/chocolatey/people), with Rob heading up the direction.

  You can host your own sources and add them to Chocolatey, you can extend Chocolatey's capabilities, and folks, it's only going to get better.

  ### Information

   * [Chocolatey Website and Community Package Repository](https://chocolatey.org)
   * [Mailing List](http://groups.google.com/group/chocolatey) / [Release Announcements Only Mailing List](https://groups.google.com/group/chocolatey-announce) / [Build Status Mailing List](http://groups.google.com/group/chocolatey-build-status)
   * [Twitter](https://twitter.com/chocolateynuget) / [Facebook](https://www.facebook.com/ChocolateySoftware) / [Github](https://github.com/chocolatey)
   * [Blog](https://chocolatey.org/blog) / [Newsletter](https://chocolatey.us8.list-manage1.com/subscribe?u=86a6d80146a0da7f2223712e4&id=73b018498d)
   * [Documentation](https://chocolatey.org/docs) / [Support](https://chocolatey.org/support)

  ### Commands
  There are quite a few commands you can call - you should check out the [command reference](https://chocolatey.org/docs/commands-reference). Here are the most common:

   * Help - choco -? or choco command -?
   * Search - choco search something
   * List - choco list -lo
   * Config - choco config list
   * Install - choco install baretail
   * Pin - choco pin windirstat
   * Outdated - choco outdated
   * Upgrade - choco upgrade baretail
   * Uninstall - choco uninstall baretail

  #### Alternative installation sources:
   * Install ruby gem - choco install compass -source ruby
   * Install python egg - choco install sphynx -source python
   * Install windows feature - choco install IIS -source windowsfeatures
   * Install webpi feature - choco install IIS7.5Express -source webpi

  #### More
  For more advanced commands and switches, use `choco -?` or `choco command -h`. You can also look at the [command reference](https://chocolatey.org/docs/commands-reference), including how you can force a package to install the x86 version of a package.

  ### Create Packages?
  We have some great guidance on how to do that. Where? I'll give you a hint, it rhymes with socks! [Docs!](https://chocolatey.org/docs/create-packages)

  In that mess there is a link to the [PowerShell Chocolatey module reference](https://chocolatey.org/docs/helpers-reference).
 Release Notes: See all - https://github.com/chocolatey/choco/blob/stable/CHANGELOG.md

chocolatey-core.extension 1.3.5.1
 Title: Chocolatey Core Extensions | Published: 22/10/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/chocolatey/chocolatey-coreteampackages/tree/master/extensions/chocolatey-core.extension
 Tags: chocolatey core extension admin
 Software Site: https://github.com/chocolatey/chocolatey-coreteampackages
 Software License: https://github.com/chocolatey/chocolatey-coreteampackages/blob/master/LICENSE.md
 Software Source: https://github.com/chocolatey/chocolatey-coreteampackages
 Documentation: https://github.com/chocolatey/chocolatey-coreteampackages/tree/master/extensions/chocolatey-core.extension/README.md
 Issues: https://github.com/chocolatey/chocolatey-coreteampackages/issues
 Summary: Helper functions extending core choco functionality
 Description: This package provides helper functions installed as a Chocolatey extension.
  These functions may be used in Chocolatey install/uninstall scripts by declaring this package a dependency in your package's nuspec.
 Release Notes: https://github.com/chocolatey/chocolatey-coreteampackages/tree/master/extensions/chocolatey-core.extension/CHANGELOG.md

chocolateygui 0.17.2
 Title: Chocolatey GUI | Published: 22/10/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/chocolatey/ChocolateyGUI/tree/develop/nuspec/chocolatey
 Tags: chocolateygui chocolatey admin foss
 Software Site: https://github.com/chocolatey/ChocolateyGUI
 Software License: https://raw.githubusercontent.com/chocolatey/ChocolateyGUI/develop/LICENSE.txt
 Software Source: https://github.com/chocolatey/ChocolateyGUI
 Issues: https://github.com/chocolatey/ChocolateyGUI/issues
 Summary: A GUI for Chocolatey
 Description: Chocolatey GUI is a delicious GUI on top of the Chocolatey command line tool.

  ## Features

  * View all **installed** and **available** packages
  * **Upgrade** installed, but outdated packages
  * **Install** and **uninstall** packages
  * See detailed **package information**

  ## Package Parameters

  - `/ShowConsoleOutput` - Enables/disables whether or not Chocolatey GUI shows output from the commands being executed when a job is running
  - `/DefaultToTileViewForLocalSource` - Enables/disables whether or not Chocolatey GUI defauls to tile instead of list view for the local source view
  - `/DefaultToTileViewForRemoteSource` - Enables/disables whether or not Chocolatey GUI defauls to tile instead of list view for all remote source views
  - `/UseDelayedSearch` - Enables/disables whether or not Chocolatey GUI uses a live search, which returns results after a short delay without clicking the search button
  - `/ExcludeInstalledPackages` - Enables/disables whether or not Chocolatey GUI shows packages that are already installed when viewing sources
  - `/ShowAggregatedSourceView` - Enables/disables whether or not Chocolatey GUI shows an additional source combining all sources into one location
  - `/OutdatedPackagesCacheDurationInMinutes` - The length of time, in minutes, which Chocolatey GUI will wait before invalidating the cached result of outdated packages for the machine

  As an example, the following installation command could be used to enable `ShowConsoleOutput` to ensure `UseDelayedSearch` is disabled, and set the output cache to 120 minutes:

  `choco install chocolateygui --params="'/ShowConsoleOutput=$true /UseDelayedSearch=$false /OutdatedPackagesCacheDurationInMinutes=120'"
 Release Notes: All release notes for Chocolatey GUI can be found on the GitHub site - https://github.com/chocolatey/ChocolateyGUI/releases

DotNet4.5.2 4.5.2.20140902
 Title: Dot Net 4.5.2 | Published: 22/10/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: n/a
 Tags: Microsoft .net Framework 4.5.2
 Software Site: http://www.microsoft.com/en-au/download/details.aspx?id=42642
 Software License: http://msdn.microsoft.com/en-US/cc300389.aspx
 Summary: The Microsoft .NET Framework 4.5.2
 Description: The Microsoft .NET Framework 4.5.2 is a highly compatible, in-place update to the Microsoft .NET Framework 4, Microsoft .NET Framework 4.5 and Microsoft .NET Framework 4.5.1

lessmsi 1.7.0
 Title: LessMSI | Published: 02/11/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/activescott/lessmsi
 Tags: msi extract extraction install
 Software Site: https://lessmsi.activescott.com/
 Software License: https://www.opensource.org/licenses/mit-license.php
 Software Source: https://github.com/activescott/lessmsi
 Documentation: https://github.com/activescott/lessmsi/wiki
 Mailing List: https://github.com/activescott/lessmsi/issues
 Issues: https://github.com/activescott/lessmsi/issues
 Summary: LessMSI - Easily extract the contents of an MSI
 Description: LessMSI is a utility with a graphical user interface and a command line interface that can be used to view and extract the contents of an MSI file. For Windows. Usage on the command line: lessmsi x msiFileName [outDir]
 Release Notes: https://github.com/activescott/lessmsi/releases

webpicmd 7.1.50430.20141001
 Title: WebPI (Platform Installer) Command Line | Published: 02/11/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: n/a
 Tags: webpi web pi platform installer microsoft tool
 Software Site: http://www.iis.net/learn/install/web-platform-installer/web-platform-installer-v4-command-line-webpicmdexe-rtw-release
 Software License: http://www.microsoft.com/web/webpi/eula/webpi_45_en.htm
 Summary: Web PI (Platform Installer) Command Line v5
 Description: The Microsoft Web Platform Installer (Web PI) Command Line v5 makes it easy for you to download, install, and keep up to date on the latest software components of the Microsoft Web Platform for development and application hosting on the Windows operating system. Web PI does the work of comparing the newest available components across the Microsoft Web Platform against what is already installed on your computer; you can see what is new and what you haven't yet installed. You can use Web PI to learn more about different components and install one or more components in a chained installation, with Web PI handling reboots and logging failures where applicable.

6 packages installed.

Did you know Pro / Business automatically syncs with Programs and
 Features? Learn more about Package Synchronizer at
 https://chocolatey.org/compare
```

## 3. Partage de fichiers

**Démarche :**
-sur le powershell de mon windows :

```
New-Item "C:\SharedFolder" -itemType Directory
$ New-SmbShare -Name SharedFolder1 -Path "C:\SharedFolder" -FullAccess "Tout le monde"
```

-sur la vm :

```
$ yum install -y cifs-utils
$ mkdir C:\SharedFolder
mount -t cifs -o username=romai,password=[...] //192.168.120.1/SharedFolder1 /SharedFile
[root@localhost SharedFile]# touch test.txt
[root@localhost SharedFile]# ls
test.txt
```

de retour sur windows powershell :

```
PS C:\SharedFolder> ls


    Répertoire : C:\SharedFolder


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        02/11/2020     18:42              0 test.txt
```
