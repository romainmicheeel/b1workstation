#Romain Michel
#22/10/2020
#ce script peut effectuer des actions après un certain temps en fonction des arguments (ici lock et shutdown)

if ($args[0] -match "lock") {
    Start-Sleep -Seconds $args[1]
    rundll32.exe user32.dll, LockWorkStationlok
}
elseif ($args -match "shutdown") {
    shutdown /s /t $args[1]
}
else {
    echo "wrong arguments"
}



